import {createStore, combineReducers, applyMiddleware, compose} from 'redux';
import getOptionStrategiesReducer from '../redux/reducer/option-reducer';
import sumbmitOptionStrategiesReducer from '../redux/reducer/options-submission-reducer';
import thunk from 'redux-thunk';
import logger from 'redux-logger';

// import your app reducers here

export default function configureStore(initialState) {
    console.log('am here in store ::: ');
    let reducer = combineReducers({
        // optionChain: getOptionStrategiesReducer,
        optionStrategies: sumbmitOptionStrategiesReducer
    });

let enhancements = [applyMiddleware(thunk)];

if (process.env.NODE_ENV !== 'production' && typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION__) {
    enhancements.push(window.__REDUX_DEVTOOLS_EXTENSION__());
}
return createStore(reducer, initialState, compose(...enhancements));
}
