import {GET_OPTION_CHAIN_STATUS, ERROR_THROWN} from '../../option-stratgies/constants/constants';
import axios from 'axios';
export const submitOptionChainActions = (stockSymbol, strikePrice, lowerStrikePrice, higherStrikePrice,
    expirationDate, marketStrategies, optionStrategies ) => {
    console.log('stockSymbol :: ', stockSymbol);
    console.log('strikePrice :: ', strikePrice);
    console.log('expirationDate :: ', expirationDate);
    console.log('marketStrategies :: ', marketStrategies);
    console.log('optionStrategies :: ', optionStrategies);
    console.log('lowerStrikePrice :: ', lowerStrikePrice);
    console.log('higherStrikePrice :: ', higherStrikePrice);
return dispatch => {
    dispatch({
        type: GET_OPTION_CHAIN_STATUS.REQUEST
    });

    // let url = 'https://apidojo-yahoo-finance-v1.p.rapidapi.com/stock/v2/get-options';
    let url = 'http://40.121.248.232:8080/v1/finance/options?market=Bull&strategy=Long+Call+Strategy&symbol=IBM&expirationDate=2021-03-09&strikePrice=50.00';
    // params: {symbol: stockSymbol, date: '1562284800', region: 'US'},
    console.log('am here 1 :: ');
   let localConfig = {
      method: 'GET',
    //    params: {symbol: stockSymbol},
       headers: {
        // 'x-rapidapi-key': '1a85a85de3msh07acd612b8a7239p18667djsn77c6bf2c52bc',
        'x-rapidapi-host': 'apidojo-yahoo-finance-v1.p.rapidapi.com',
        'Access-Control-Allow-Origin' : '*'
      }
   }

    axios.get(url, localConfig).then(response => {
        console.log('response is :: ', response);
        dispatch({
            type: GET_OPTION_CHAIN_STATUS.SUCCESS,
            payload: response.data
            // optionStrategies: optionStrategies
        });
    }).catch(error => {
        dispatch({
            type: ERROR_THROWN,
            payload: GET_OPTION_CHAIN_STATUS
        });
    })
   

}
}