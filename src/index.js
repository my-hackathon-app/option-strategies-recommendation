import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import 'bootstrap/dist/css/bootstrap.css';
import './index.css';
import EntryPage from './option-stratgies/entry-page';
import ResultsPage from './option-stratgies/result-page';
import App from './App';
import {BrowserRouter, Switch, Link, Route} from 'react-router-dom';
import * as serviceWorker from './serviceWorker';
import configureStore from './redux/configureStore';

let store = configureStore();
ReactDOM.render(
  <Provider store={store}>
  <BrowserRouter>
  <div>
  <Switch>
    <Route exact path='/' component={EntryPage}/>
    <Route path='/entry-page' component={EntryPage}/>
    <Route path='/results-page' component={ResultsPage}/>
    </Switch>
  </div>
  </BrowserRouter>
  </Provider>,
     
  document.getElementById('root')
);

serviceWorker.unregister();
