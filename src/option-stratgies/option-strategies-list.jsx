export const optionStrategiesList = [
    {
     Bullish: [
         {
                v1: 'Long Call Strategy',
                v2: 'Short Put Strategy',
                v3: 'Bull Put Strategy',
                v4: 'Call Back -Spread Strategy'
            }  
    
     ],
     Bearish: [
         {
             v1: 'Long Put Strategy',
             v2: 'Short Call Strategy',
             v3: 'Bear Call Strategy',
             v4: 'Bear Put Strategy'
            }  

    ]
    
    }
];

export default optionStrategiesList;
                        
