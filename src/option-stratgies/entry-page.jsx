import React from 'react';
import PropTypes from 'prop-types';
import 'bootstrap/dist/css/bootstrap.min.css';
import {reduxForm} from 'redux-form';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Form} from 'react-bootstrap';
import BottomFooter from './bottom-footer';
import BullBearImg from '../images/option-stratagies.png';
import BullBearImg1 from '../images/4.jpg';
import marketImg from '../images/7.jpg';
import {getOptionChainActions} from '../redux/actions/option-action';
import {submitOptionChainActions} from '../redux/actions/options-submission-action'; 
import Dropdown from 'react-bootstrap/Dropdown';
import Spinner from 'react-bootstrap/Spinner';
import {optionStrategiesList} from './option-strategies-list';
import {MARKET_STRATEGIES} from '../option-stratgies/constants/constants';

export class EntryPage extends React.Component {
    constructor (props) {
        super(props);
        this.handleMarketStrategies = this.handleMarketStrategies.bind(this);
        this.handleOptionStrategies = this.handleOptionStrategies.bind(this);
        this.submitAction = this.submitAction.bind(this);
        this.handleStockValue = this.handleStockValue.bind(this);
        this.handleStrikeValue = this.handleStrikeValue.bind(this);
        this.handleLowerStrikeValue = this.handleLowerStrikeValue.bind(this);
        this.handleHigherStrikeValue = this.handleHigherStrikeValue.bind(this);
        this.handleExpirationDate = this.handleExpirationDate.bind(this);
    }

handleMarketStrategies=(e)=>{
    this.setState({
        marketStrategies: e,
        optionStrategies: this.props.optionStrategies
    });
  }
  
  
  handleOptionStrategies=(e)=>{
    this.setState({
        optionStrategies: e
    });


  }

handleStockValue(event) {
    // console.log('event.target.value :: ',event.target.value);
    this.setState({
        stockSymbol: event.target.value
    });
}

handleStrikeValue(event) {
    // console.log('event.target.value :: ',event.target.value);
    this.setState({
        strikePrice: event.target.value
    });
}

handleLowerStrikeValue(event) {
    // console.log('event.target.value :: ',event.target.value);
    this.setState({
        lowerStrikePrice: event.target.value
    });
}


handleHigherStrikeValue(event) {
    // console.log('event.target.value :: ',event.target.value);
    this.setState({
        higherStrikePrice: event.target.value
    });
}


handleExpirationDate(event) {
    this.setState({
        expirationDate: event.target.value
    });
}

fetchOptionStrategiesList (marketStrategies, optionStrategiesList) {
let strategies = '';
if (null !== marketStrategies && null !== optionStrategiesList) {
    optionStrategiesList.map(option => {
        if(marketStrategies === MARKET_STRATEGIES.BULLISH) {
            strategies = option.Bullish;
        } else {
            strategies = option.Bearish;
        } 
    })

}
return strategies;
}


optionStrategiesDropdownValues(optionStrategiesValues) {
    // console.log(optionStrategiesValues.v1);
let dropdownval = '';
    {optionStrategiesValues.forEach((option) => {
        dropdownval = (<Dropdown.Menu>
              <Dropdown.Item eventKey={option.v1} onSelect={this.handleOptionStrategies}>{option.v1}</Dropdown.Item> 
              <Dropdown.Item eventKey={option.v2} onSelect={this.handleOptionStrategies}>{option.v2}</Dropdown.Item> 
              <Dropdown.Item eventKey={option.v3} onSelect={this.handleOptionStrategies}>{option.v3}</Dropdown.Item> 
              <Dropdown.Item eventKey={option.v4} onSelect={this.handleOptionStrategies}>{option.v4}</Dropdown.Item>      
         </Dropdown.Menu>)
    })}

    return dropdownval;
}

submitAction() {
// console.log('this.state.stockSymbol is :: ',this.state);
if(this.state !== null && this.state.stockSymbol !== null && this.state.strikePrice !== null && this.state.expirationDate !== null
    && this.state.marketStrategies !== null && this.state.optionStrategies !== null) {

    this.props.submitOptionChainActions(this.state.stockSymbol, this.state.strikePrice,this.state.lowerStrikePrice,
        this.state.higherStrikePrice, this.state.expirationDate, this.state.marketStrategies, this.state.optionStrategies);
{/* <Spinner
      as="span"
      animation="grow"
      size="sm"
      role="status"
      aria-hidden="true"
    ></Spinner>
     */}


        this.props.history.push('/results-page');
}
}
    render() {
        let {optionStrategies, marketStrategies} = this.props;
        if(this.state != null && this.state.marketStrategies != null) {
            marketStrategies = this.state.marketStrategies;
            optionStrategies = this.state.optionStrategies;
        } 

        let putSpreadStatus = false;
        let callSpreadStatus = false;

        if((marketStrategies === MARKET_STRATEGIES.BULLISH && optionStrategies === 'Bull Put Strategy') || 
           (marketStrategies === MARKET_STRATEGIES.BEARISH && optionStrategies === 'Bear Put Strategy')) {
            putSpreadStatus = true;
        }

        if((marketStrategies === MARKET_STRATEGIES.BULLISH && optionStrategies === 'Call Back -Spread Strategy') || 
        (marketStrategies === MARKET_STRATEGIES.BEARISH && optionStrategies === 'Bear Call Strategy')){
            callSpreadStatus = true;
        }

       let optionStrategiesValues = this.fetchOptionStrategiesList(marketStrategies, optionStrategiesList);
        
        return(
            <div>
                <div>
                    <form>
                    
                    {/* <header>Welcome to Option Strategies Reccommendation Application</header> */}

                    <nav class="navbar navbar-inverse">
                            <div class="container-fluid">
                            <div class="navbar-header">
                                {/* <a class="navbar-brand" href="#">Home</a> */}
                            </div>
                            <div class="collapse navbar-collapse" id="myNavbar">
                                <ul class="nav navbar-nav">
                                {/* <li class="active"><a href="#">Home</a></li> */}
                                </ul>
                            </div>
                            </div>
                        </nav>
                        <div class="container">
    <div class="row">
    <div class="col-sm-12 text-center" id="heading-text">
            <h1>Options Strategies Recommendation</h1>
            
        </div>
        <div class="col-sm-12 text-center bullbear-align"> 
            <img src={BullBearImg} class="img-rounded" width="350" height="auto"></img>
        </div>

        {/* <div class="column"> 
            <img src={BullBearImg1} class="img-rounded" width="350" height="auto"></img>
        </div> */}
        
    </div>

    <div class="container">
    <div class="row">
    <div class="col-sm-2">
    </div>

    <div class="col-sm-8">
        <Form>

        <div class="row">
            <div class="col-sm-4 market-dropdown">
            <Form.Label class="form-label">Market Strategies * </Form.Label>
        <Form.Group controlId="marketStrategies">
        <Dropdown>
                <Dropdown.Toggle variant="success" id="dropdown-basic" >
                    {marketStrategies}
                </Dropdown.Toggle>

                <Dropdown.Menu>
                    <Dropdown.Item eventKey="Bullish" onSelect={this.handleMarketStrategies}>Bullish</Dropdown.Item>
                    <Dropdown.Item eventKey="Bearish" onSelect={this.handleMarketStrategies}>Bearish</Dropdown.Item>
                </Dropdown.Menu>
        </Dropdown>

        </Form.Group>
            </div>
            <div class="col-sm-8 option-dropdown">
            <Form.Label class="form-label">Options Strategies * </Form.Label>
        <Form.Group controlId="optionStrategies">
<       Dropdown>
                <Dropdown.Toggle variant="success" id="dropdown-basic">
                    {optionStrategies}
                </Dropdown.Toggle>
                {this.optionStrategiesDropdownValues(optionStrategiesValues)}
                
        </Dropdown>
        </Form.Group>
            </div>
    </div>
        <div class="">
        <Form.Group controlId="stockSymbol">
        <Form.Label class="form-label">Stock / Index Symbol * </Form.Label>
        
        <Form.Control required type="text" name="stockSymbol" placeholder="Enter Stock Symbol" 
        onChange={this.handleStockValue}/>
        </Form.Group>

        {!putSpreadStatus && !callSpreadStatus && <Form.Group controlId="strikePrice">
            <Form.Label class="form-label">Strike Price * </Form.Label>
            <Form.Control required type="number" name="strikePrice" placeholder="Enter Strike Price"
            onChange={this.handleStrikeValue} />   
        </Form.Group>}

        {putSpreadStatus && <Form.Group controlId="strikePrice">
            <Form.Label class="form-label">At-the-Money Put – Higher Strike Price * </Form.Label>
            <Form.Control required type="number" name="higherStrikePrice" placeholder="Enter Higher Strike Price"
            onChange={this.handleHigherStrikeValue} />           
        </Form.Group>}

        {putSpreadStatus && <Form.Group controlId="strikePrice">
            <Form.Label class="form-label">Out-the-Money Put – Lower Strike Price * </Form.Label>
            <Form.Control required type="number" name="lowerStrikePrice" placeholder="Enter Lower Strike Price"
            onChange={this.handleLowerStrikeValue} />            
        </Form.Group> }

        {callSpreadStatus && <Form.Group controlId="strikePrice">
            <Form.Label class="form-label">At-the-Money Call - Strike Price * </Form.Label>
            <Form.Control required type="number" name="higherStrikePrice" placeholder="Enter Higher Strike Price"
            onChange={this.handleHigherStrikeValue} />           
        </Form.Group>}

        {callSpreadStatus && <Form.Group controlId="strikePrice">
            <Form.Label> class="form-label"Out-the-Money (OTM) Call –Strike Price * </Form.Label>
            <Form.Control required type="number" name="lowerStrikePrice" placeholder="Enter Lower Strike Price"
            onChange={this.handleLowerStrikeValue} />            
        </Form.Group> }


        <Form.Group controlId="dateOfExpiration">
            <Form.Label class="form-label">Expiration Date*</Form.Label>
            <Form.Control required type="date" name="expirationDate" placeholder="Date of Expiration" 
            onChange={this.handleExpirationDate} />
            </Form.Group>
    </div>
    <div class="entry-submit-button">
    <button type="button" class="btn btn-info btn-lg" onClick={this.submitAction}>Submit</button>
    </div>
    
    </Form>
    </div>
        </div>
        <br>
        </br>
        
        <BottomFooter/>     
    </div>
</div>

<div class="footer-copyright text-center py-3">© 2021 Copyright:
        <a href="https://www.optionsstrategies.com/"> www.optionsstrategies.com</a>
    </div>

                    </form>
                </div>
            </div>
        );
    }
}

EntryPage.propTypes = {
    getOptionChainActions: PropTypes.func,
    fetchOptionStrategiesList: PropTypes.func,
    handleOptionStrategies: PropTypes.func,
    handleMarketStrategies: PropTypes.func,
    history: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func,
    submitAction: PropTypes.func,
    submitOptionChainActions: PropTypes.func,
    optionStrategies: PropTypes.object,
    marketStrategies: PropTypes.object,
    stockSymbol: PropTypes.object,
    strikePrice: PropTypes.object
}


function mapStateToProps(state) {
    console.log('am here state :: ',state);
    return {
        optionChain: state.optionChain,
        marketStrategies: 'Select the Market Strategies',
        optionStrategies: 'Select the Option Strategies'
    }
}

const mapDispatchToProps = dispatch => {
return bindActionCreators (
    {
    dispatch,
    getOptionChainActions,
    submitOptionChainActions
    },
    dispatch
);
}; 


export default connect(mapStateToProps, mapDispatchToProps) (
    reduxForm({
        form: 'myOptionStrategiesRecommendForm',
        destryOnUnmount: false
    }) (EntryPage)
);