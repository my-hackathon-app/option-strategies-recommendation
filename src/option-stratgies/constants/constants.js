export const GET_OPTION_CHAIN_STATUS = {
    REQUEST: 'GET_OPTION_CHAIN_REQUEST', 
    SUCCESS: 'GET_OPTION_CHAIN_SUCCESS'
}

export const POST_OPTION_CHAIN_STATUS = {
    REQUEST: 'POST_OPTION_CHAIN_REQUEST', 
    SUCCESS: 'POST_OPTION_CHAIN_SUCCESS'
}

export const ERROR_THROWN = 'ERROR_THROWN';

export const RESULTS_PAGE_HEADING = 'Options Strategies Results Page';

export const resultsColumnHeading = [
    { name: 'Current Stock Market Price' },
    { name: 'Strike Price' },
    { name: 'Premium Paid' },
    { name: 'Break Even Premium \n (Strike Price + Premium Paid)'},
    { name: 'Lot Size' },
    { name: 'Open Interest'},
    { name: 'Volume' },
    { name: 'Bid' },
    { name: 'Ask' },
    { name: 'Implied Volatility' },
    { name: 'Chance of Profit' }
  ];

  export const MARKET_STRATEGIES = {
          BULLISH: 'Bullish',
          BEARISH: 'Bearish'
      };
  