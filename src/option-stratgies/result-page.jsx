import React from 'react';
import PropTypes from 'prop-types';
import 'bootstrap/dist/css/bootstrap.min.css';
import BullBearImg from '../images/option-stratagies.png';
import BottomFooter from './bottom-footer';
import Table from 'react-bootstrap/Table';
// import {reduxForm} from 'redux-form';
import {connect} from 'react-redux';
// import {bindActionCreators} from 'redux';
import {retrieveStrategies, retrievePutStrategies} from './strategies-functions';
import {resultsColumnHeading, RESULTS_PAGE_HEADING} from '../option-stratgies/constants/constants';


export class ResultsPage extends React.Component {
    constructor (props) {
        super(props);   
        this.submitAction = this.submitAction.bind(this);
        this.resultPageList = this.resultPageList.bind(this)
    }

    resultPageList(strategies) {

    }

    submitAction() {
        // console.log('am here to submit', this.props.strikePrice);
        
        // this.props.submitOptionChainActions();
        this.props.history.push('/');
        }

    render() {
        let currentStockMarketPrice, strikePrice, premiumPaid, breakEvenPremium, lotSize, openIntrest = '';
        let volume, ask, bid, impliedVelocity, chanceofProfit = '';
        // console.log("optionQuote is :: ", this.props.optionQuote);
        // console.log("strategies is :: ", this.props.strategies);
        // console.log("callStrategy is :: ", OPTION_STRATEGIES);

        
        //   if(this.props.callStrategy != null && this.props.callStrategy != '') {
        //     let stre = retrievePutStrategies(this.props.callStrategy);

        //   }
       
        return(
            <div>
                <div>
                <form>
                    
                    <nav class="navbar navbar-inverse">
                            <div class="container-fluid">
                            <div class="navbar-header">
                                
                               <a class="navbar-brand" href="#">Home</a>
                            </div>
                            <div class="collapse navbar-collapse" id="myNavbar">
                                <ul class="nav navbar-nav">
                                <li class="active"><a href="#">Home</a></li>
                                </ul>
                                
                            </div>
                            </div>
                        </nav>
                        <div class="container">
                        <div class="row">
                        <div class="col-sm-12 text-center"> 
                            <img src={BullBearImg} class="img-rounded bullbear-align" width="350" height="auto"></img>
                        </div>
                        
            </div>
            <div class="col-sm-12 text-center" id="heading-text">
                            <h2 class="text-align">{RESULTS_PAGE_HEADING}</h2>
                            <Table striped bordered hover size="sm" class="result-table">
                        <thead>
                            <tr>
                            <th>S.No</th>
                            <th>{resultsColumnHeading[0].name}</th>
                            <th>{resultsColumnHeading[1].name}</th>
                            <th>{resultsColumnHeading[2].name}</th>
                            <th>{resultsColumnHeading[3].name}</th>
                            <th>{resultsColumnHeading[4].name}</th>
                            <th>{resultsColumnHeading[5].name}</th>
                            <th>{resultsColumnHeading[6].name}</th>
                            <th>{resultsColumnHeading[7].name}</th>
                            <th>{resultsColumnHeading[8].name}</th>
                            <th>{resultsColumnHeading[9].name}</th>
                            {/* <th>{resultsColumnHeading[10].name}</th> */}
                            </tr>
                        </thead>
                        <tbody>
                        {this.props.strategies && this.props.strategies.map((item, index) => {
                            console.log('item[0] :: ',item)
                            return (
                            <tr>
                                <td>{index}</td>
                                <td>{item.currentMarketPrice}</td>
                                <td>{item.strikePrice}</td>
                                <td>{''}</td>
                                <td>{''}</td>
                                <td>{item.lotSize}</td>
                                <td>{item.openInterest}</td>
                                <td>{item.volume}</td>
                                <td>{item.bid}</td>
                                <td>{item.ask}</td>
                                <td>{item.impliedVolatility}</td>
                                
                            </tr>
                            );
                        })}
                        </tbody>
                        </Table>
                        </div>
                        <div class="submit-button">
                        <button type="button" class="btn btn-info btn-lg" onClick={this.submitAction}>Home</button>
                        </div>
                        </div>
             </form>
                </div>
                <BottomFooter/>

                <div class="footer-copyright text-center py-3">© 2021 Copyright:
                    <a href="https://www.optionsstrategies.com/"> www.optionsstrategies.com</a>
                </div>
            </div>
             
        );
    }
}

ResultsPage.propTypes = {
    history: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func,
    submitAction: PropTypes.func,
    retrieveStrategies: PropTypes.func,
    retrievePutStrategies: PropTypes.func
}


function mapStateToProps(state) {
    console.log('state :: ', state);
    return {
        strategies: state.optionStrategies,
    }
}

/*function mapDispatchToProps = dispatch => {
return bindActionCreators ({

})
} */


/*export default connect (mapStateToProps) (
    reduxForm({
        form: 'myOptionStrategiesRecommendForm',
        destryOnUnmount: false
    }) (ResultsPage)
);*/

export default connect (mapStateToProps) (ResultsPage);