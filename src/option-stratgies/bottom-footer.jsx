import React from 'react';


export class BottomFooter extends React.Component{

    render(){

        return(
            <div>
                <div class="text-md-left">
                <div class="row">
                    <div class="col-md-3 mx-auto">
                    {/* <h5 class="text-uppercase"><strong>Popular Locations</strong></h5> */}
                    <ul class="list-unstyled">
                        {/* <li>
                        <a href="#!">Chennai</a>
                        </li>
                        <li>
                        <a href="#!">Mumbai</a>
                        </li>
                        <li>
                        <a href="#!">Madurai</a>
                        </li> */}
                    </ul>
                    </div>
                <div class="col-md-3 mx-auto">
                    {/* <h5 class="text-uppercase"><strong>Abouts us</strong></h5> */}
                    <ul class="list-unstyled">
                        {/* <li>
                            <a href="#!">About Options Strategies</a>
                        </li>
                        <li>
                            <a href="#!">Careers</a>
                        </li>
                        <li>
                            <a href="#!">Contact Us</a>
                        </li> */}
                    </ul>
                    </div>
                    <div class="col-md-3 mx-auto">
                        {/* <h5 class="text-uppercase"><strong>Options Strategies Recommendation</strong></h5>
                        <ul class="list-unstyled">
                        <li>
                            <a href="#!">Help</a>
                        </li>
                        <li>
                            <a href="#!">Sitemap</a>
                        </li>
                        <li>
                            <a href="#!">Legacy & Privacy information</a>
                        </li> */}
                        {/* </ul> */}
                    </div>
                </div>
                </div>
            </div>
        )
    }
    
}

export default BottomFooter;